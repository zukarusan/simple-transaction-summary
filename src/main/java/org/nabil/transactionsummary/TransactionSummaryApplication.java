package org.nabil.transactionsummary;

import org.nabil.transactionsummary.service.TransactionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration
public class TransactionSummaryApplication {
	@Autowired
	Environment env;
	@Value("${org.nabil.transactionsummary.pagingSize}")
	int DEFAULT_PAGING_SIZE;
	public static void main(String[] args) {
		SpringApplication.run(TransactionSummaryApplication.class, args);
	}

	@Bean
	public TransactionRecordService transactionRecordService() {
		return new TransactionRecordService();
	}
}
