package org.nabil.transactionsummary.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.springframework.data.annotation.Transient;

import java.time.LocalDateTime;

@Entity
@Immutable
@Subselect("""
with credits as (
    SELECT
        value
    FROM\s
        TransactionRecord
	Where type = 1
),
debits as (
    SELECT
        value
    FROM\s
        TransactionRecord
	Where type = 0
),
credits_quartiles as (
    SELECT
        value,
        NTILE(4) OVER (ORDER BY value asc) AS quartile
    FROM\s
        credits
),
debits_quartiles as (
    SELECT
        value,
        NTILE(4) OVER (ORDER BY value asc) AS quartile
    FROM\s
        debits
),
Q1_credits as (
	select max(value) as value
    from credits_quartiles
    where quartile = 1
),
Q1_debits as (
	select max(value) as value
    from debits_quartiles
    where quartile = 1
),
Q3_credits as (
	select min(value) as value
    from credits_quartiles
    where quartile = 3
),
Q3_debits as (
	select min(value) as value
    from debits_quartiles
    where quartile = 3
),
credits_bounds as (
	SELECT\s
		1 as type,
		Q3_credits.value + (1.5 * (Q3_credits.value - Q1_credits.value)) AS upper_bound_outlier,
		Q1_credits.value - (1.5 * (Q3_credits.value - Q1_credits.value)) AS lower_bound_outlier
	FROM Q1_credits
	Left Join Q3_credits on Q1_credits.value < Q3_credits.value
),
debits_bounds as (
	SELECT\s
		0 as type,
		Q3_debits.value + (1.5 * (Q3_debits.value - Q1_debits.value)) AS upper_bound_outlier,
		Q1_debits.value - (1.5 * (Q3_debits.value - Q1_debits.value)) AS lower_bound_outlier
	FROM Q1_debits
	Left Join Q3_debits on Q1_debits.value < Q3_debits.value
)
Select tr.id, tr.value, tr.type,
	case\s
    when (tr.type = 1 and tr.value < cd.lower_bound_outlier or tr.value > cd.upper_bound_outlier) or
		(tr.type = 0 and tr.value < bd.lower_bound_outlier or tr.value > bd.upper_bound_outlier)\s
        then 1\s
    else 0\s
	end as is_outlier
from TransactionRecord tr
Left Join credits_bounds as cd on tr.type = cd.type
Left Join debits_bounds as bd on tr.type = bd.type
        """)
public class TransactionOutlier {
    @Id
    @Column
    private Long id;
    @Transient
    @Column(name = "is_outlier")
    Boolean isOutlier;
    @Column
    @Transient
    private Double value;
    @Column()
    @Transient
    private Boolean type;

    public Long getId() {
        return id;
    }
    public Boolean getOutlier() {
        return isOutlier;
    }

}
