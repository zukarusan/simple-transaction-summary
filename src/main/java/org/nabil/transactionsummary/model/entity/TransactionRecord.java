package org.nabil.transactionsummary.model.entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name="TransactionRecord")
public class TransactionRecord {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private Double value;
    @Column(name = "timestamp", insertable = false, updatable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date recorded;
    @Column(nullable = false)
    private Boolean type;
    @Column
    private String category;
    public void recordTimestamp() {
        this.recorded = new Date();
    }

    @Transient
    public String transactionType() {
        return type ? "credit" : "debit";
    }
    public Long getId() {
        return id;
    }

    public Double getValue() {
        return value;
    }

    public Date recordedAt() {
        return recorded;
    }

    public Boolean getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public void setCategory(String category) {
        this.category = category;
    }


}
