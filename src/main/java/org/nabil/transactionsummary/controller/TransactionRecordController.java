package org.nabil.transactionsummary.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.nabil.transactionsummary.model.entity.TransactionOutlier;
import org.nabil.transactionsummary.model.entity.TransactionRecord;
import org.nabil.transactionsummary.service.TransactionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionRecordController {

    @Autowired
    TransactionRecordService recordService;
    @ExceptionHandler({NoHandlerFoundException.class})
    public ResponseEntity<ErrorResponse> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpServletRequest httpServletRequest) {
        ErrorResponse error = new ErrorResponseException(HttpStatus.NOT_FOUND, ex);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body(error);
    }
    @PostMapping("/record")
    public ResponseEntity<TransactionRecord> recordTransaction(@RequestBody TransactionRecord record) {
        TransactionRecord savedRecord = recordService.recordTransaction(record);
        return new ResponseEntity<>(savedRecord, HttpStatus.CREATED);
    }

    @PostMapping("/record/batch")
    public ResponseEntity<List<TransactionRecord>> recordTransactions(@RequestBody List<TransactionRecord> records) {
        List<TransactionRecord> savedRecords = recordService.recordTransactions(records);
        return new ResponseEntity<>(savedRecords, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeRecord(@PathVariable long id) {
        recordService.removeRecord(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/category/{category}")
    public ResponseEntity<Void> removeRecordByCategory(@PathVariable String category) {
        recordService.removeRecord(category);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionRecord> getTransaction(@PathVariable long id) {
        TransactionRecord record = recordService.getTransaction(id);
        return new ResponseEntity<>(record, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<TransactionRecord>> retrieveTransactionsPaginated(Pageable pageable) {
        Page<TransactionRecord> transactions = recordService.retrieveTransactionsPaginated(pageable);
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @GetMapping("/outliers")
    public ResponseEntity<Page<TransactionOutlier>> fetchOutliers(Pageable pageable) {
        Page<TransactionOutlier> outliers = recordService.fetchOutliers(pageable);
        return new ResponseEntity<>(outliers, HttpStatus.OK);
    }

    @GetMapping("/iqr")
    public ResponseEntity<Page<TransactionOutlier>> fetchIQRRecords(Pageable pageable) {
        Page<TransactionOutlier> iqrRecords = recordService.fetchIQRRecords(pageable);
        return new ResponseEntity<>(iqrRecords, HttpStatus.OK);
    }

    @GetMapping("/revenue")
    public ResponseEntity<Double> getTotalRevenue(@RequestParam Date betweenFrom, @RequestParam Date betweenTo) {
        Double totalRevenue = recordService.getTotalRevenue(betweenFrom, betweenTo);
        return new ResponseEntity<>(totalRevenue, HttpStatus.OK);
    }
}
