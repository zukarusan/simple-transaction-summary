package org.nabil.transactionsummary.service;

import jakarta.persistence.EntityNotFoundException;
import org.nabil.transactionsummary.model.entity.TransactionOutlier;
import org.nabil.transactionsummary.model.entity.TransactionRecord;
import org.nabil.transactionsummary.repository.TransactionOutlierRepository;
import org.nabil.transactionsummary.repository.TransactionRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Scope(value = "session")
@Component(value = "transactionRecordService")
public class TransactionRecordService {
    @Autowired
    private TransactionRecordRepository repository;
    @Autowired
    private TransactionOutlierRepository outlierRepository;
    public TransactionRecord recordTransaction(TransactionRecord record) {
        record.recordTimestamp();
        return repository.saveAndFlush(record);
    }
    public List<TransactionRecord> recordTransactions(List<TransactionRecord> records) {
        records.stream()
                .forEach(TransactionRecord::recordTimestamp);
        return repository.saveAllAndFlush(records);
    }
    public void removeRecord(long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException();
        }
        repository.deleteById(id);
    }
    public void removeRecord(String category) {
        repository.deleteByCategory(category);
    }
    public TransactionRecord getTransaction(long id) {
        return repository.findById(id);
    }
    public Page<TransactionRecord> retrieveTransactionsPaginated(Pageable pageable) {
        return repository.findAll(pageable);
    }
    public Page<TransactionOutlier> fetchOutliers(Pageable pageable) {
        return outlierRepository.findAllByType(true, pageable);
    }
    public Page<TransactionOutlier> fetchIQRRecords(Pageable pageable) {
        return outlierRepository.findAllByType(false, pageable);
    }
    @Transactional(readOnly = true)
    public Double getTotalRevenue(Date betweenFrom, Date betweenTo) {
        double totalCredits, totalDebits;
        try (Stream<TransactionRecord> transactionStream = repository.streamAll()) {
            totalCredits = transactionStream
                    .filter(tr -> tr.getType() && tr.recordedAt().after(betweenFrom) && tr.recordedAt().before(betweenTo))
                    .mapToDouble(TransactionRecord::getValue)
                    .sum();
        } catch(Exception e) {
            return -1.0;
        }
        try (Stream<TransactionRecord> transactionStream = repository.streamAll()) {
            totalDebits = transactionStream
                    .filter(tr -> !tr.getType() && tr.recordedAt().after(betweenFrom) && tr.recordedAt().before(betweenTo))
                    .mapToDouble(TransactionRecord::getValue)
                    .sum();
        } catch(Exception e) {
            return -1.0;
        }
        // Calculate total debits within the specified range
        return totalCredits - totalDebits;
    }

}
