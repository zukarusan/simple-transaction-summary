package org.nabil.transactionsummary.repository;

import org.nabil.transactionsummary.model.entity.TransactionRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface TransactionRecordRepository extends JpaRepository<TransactionRecord, Long> {
    Page<TransactionRecord> findAllByCategory(String category, Pageable pageable);
    Page<TransactionRecord> findAllByType(String type, Pageable pageable);
    Page<TransactionRecord> findAll(Pageable pageable);
    Page<TransactionRecord> findAllByValueGreaterThanAndValueLessThan(double valueFromExclusive, double valueToExclusive, Pageable pageable);

    TransactionRecord findById(long id);
    @Query("select tr from TransactionRecord as tr")
    Stream<TransactionRecord> streamAll();
    void deleteByCategory(String category);
    void deleteById(long id);

}
