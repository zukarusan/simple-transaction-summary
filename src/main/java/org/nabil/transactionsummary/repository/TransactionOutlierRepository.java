package org.nabil.transactionsummary.repository;

import org.nabil.transactionsummary.model.entity.TransactionOutlier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionOutlierRepository extends JpaRepository<TransactionOutlier, Long> {
    Page<TransactionOutlier> findAllByType(Boolean type, Pageable  pageable);
}
