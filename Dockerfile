FROM openjdk:17-jdk-alpine
MAINTAINER org.nabil
COPY target/transaction-summary-0.0.1.jar transaction-summary.jar
ENTRYPOINT ["java","-jar","/transaction-summary.jar"]
